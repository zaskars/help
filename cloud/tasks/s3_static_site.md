# Развертывание статического сайта в Object Storage

::: tip Перед началом
Изучите [статью про Object Storage](/cloud/services/s3.html)
:::

[Object Storage](/cloud/services/s3.html) может не только хранить загружаемые пользователями файлы, но и служить хостингом для статических сайтов. Статический сайт - любой сайт, который реализован как набор неизменяемых файлов (например, фронтенд на HTML, CSS, JS, картинки...).

## Подготовка

Откройте бакет и перейдите в раздел **Веб-сайт**. Включите **Хостинг**, укажите главную страницу `index.html` и сохраните изменения. Появится адрес сайта бакета, а файлы, которые будут загружены в этот бакет, отобразятся на этом сайте.

## Загрузка файлов

Загрузить файлы можно [несколькими способами](https://cloud.yandex.ru/docs/storage/tools/). Например, вручную это можно сделать прямо через браузер в консоли Яндекс.Облака. 

Ниже мы рассмотрим процесс загрузки с помощью AWS CLI.

1. Установите [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
2. Установите переменные окружения
```bash
export AWS_ACCESS_KEY_ID=accesskey
export AWS_SECRET_ACCESS_KEY=secretkey
export AWS_DEFAULT_REGION=ru-central1
```

3. Выполните команду, заменив `local_folder` на папку, из которой надо загружать файлы, а `bucket-name` - на название бакета
```bash
aws --endpoint-url=https://storage.yandexcloud.net \
  s3 cp --recursive \
  local_folder/ \ 
  s3://bucket-name/
```

Чтобы автоматизировать развертывание сайта в Object Storage, эти шаги можно перенести в [Gitlab CI](/cloud/serverless/ci-cd.html).


## Подключение к API Gateway

Дополните конфигурацию [API Gateway](/cloud/serverless/api_gateway.html)

```yml{9,12,25,28}
# здесь другие настройки
paths:
  # добавляем новые пути:
  /:  # раздаем index.html
    get:
      summary: Serve static file from Yandex Cloud Object Storage
      x-yc-apigateway-integration:
        type: object_storage
        bucket: bucket_name # вставьте название бакепа
        object: index.html
        error_object: error.html
        service_account_id: ajeo... # вставьте ID сервисного аккаунта
  /{file+}:  # раздаем остальные файлы
    get:
      summary: Serve static file from Yandex Cloud Object Storage
      parameters:
        - name: file
          explode: false
          in: path
          required: true
          schema:
            type: string
      x-yc-apigateway-integration:
        type: object_storage
        bucket: bucket_name # вставьте название бакепа
        object: '{file}'
        error_object: error.html
        service_account_id: ajeo... # вставьте ID сервисного аккаунта
```