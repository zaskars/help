# Запуск статического сайта и бекенда под одним доменом

[API Gateway](/cloud/serverless/api_gateway.html) умеет проксировать запросы на [разные сервисы в Яндекс.Облаке](https://cloud.yandex.ru/docs/api-gateway/concepts/extensions/).

При разработке веб-приложения адреса можно организовать следующим образом:
- [фронтенд, развернутый в Object Storage](/cloud/tasks/s3_static_site.html) может размещаться на корневом адресе (`/`)
- [бекенд, развернутый в Serverless Container](/cloud/serverless/container.html) может размещаться на адресе `/api`

Вот пример конфигурации API Gateway, позволяющий реализовать такую схему (сделайте замену в выделенных строчках):

```yml{11,14,27,30,43,44}
openapi: 3.0.0
info:
  title: Web app
  version: 1.0.0
paths:
  /:  # раздаем index.html
    get:
      summary: Serve static file from Yandex Cloud Object Storage
      x-yc-apigateway-integration:
        type: object_storage
        bucket: bucketname # вставьте название бакепа
        object: index.html
        error_object: error.html
        service_account_id: serviceaccountid # вставьте ID сервисного аккаунта
  /{file+}:  # раздаем остальные файлы фронтенда
    get:
      summary: Serve static file from Yandex Cloud Object Storage
      parameters:
        - name: file
          explode: false
          in: path
          required: true
          schema:
            type: string
      x-yc-apigateway-integration:
        type: object_storage
        bucket: bucketname # вставьте название бакепа
        object: '{file}'
        error_object: error.html
        service_account_id: serviceaccountid # вставьте ID сервисного аккаунта
  /api/{url+}: # запускаем API
    x-yc-apigateway-any-method:
      summary: Execute container
      operationId: container
      parameters:
      - explode: false
        in: path
        name: url
        required: false
        style: simple
      x-yc-apigateway-integration:
        type: serverless_containers
        container_id: containerid # ID контейнера
        service_account_id: serviceaccountid # ID сервисного аккаунта

```