# Развертывание в Serverless-инфраструктуре

Serverless - подход, в котором серверами для запуска управляет облачный провайдер. В таком случае оплата ресурсов идет только за фактические потраченные ресурсы.

## Разворачивание проекта в Serverless

::: warning Копируйте примеры кода из документации
Некоторые детали в видео могли устареть
:::

<iframe width="560" height="315" src="https://www.youtube.com/embed/8miYs3DmurU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### Упакуйте проект в Docker

Тут есть несколько нюансов, описали их в [отдельной статье](/cloud/serverless/build.html)

### Отправка заявки в UniEnv

1. [Подайте заявку на размещение проекта](/)
2. <a :href="$page.CLOUD_LOGIN" target="_blank">Войдите в Яндекс.Облако</a>
3. Дождитесь одобрения заявки в UniEnv, чтобы получить доступы к облаку

### Запуск проекта в облаке 

1. [Создайте базу данных PostgreSQL в UniEnv](/deploy/unienv_db.html) (если она нужна) 
2. <a :href="$page.YANDEX_CLI_HELP" target="_blank">Установите и настройте Yandex Cloud CLI</a>
3. [Создайте Yandex Cloud Registry и отправьте образ с приложением туда](/cloud/serverless/registry.html)
4. [Создайте Serverless Container](/cloud/serverless/container.html)
5. [Создайте API Gateway](/cloud/serverless/api_gateway.html)
6. [Решите проблемы](/cloud/serverless/problems.html), если они возникли
7. Приложение развернуто!

![](../../images/cloud/serverless/diagram.png)

<div id="update"></div>

## Автоматическое развертывание

Настройка автоматического с помощью Gitlab CI описана [здесь](/cloud/serverless/ci-cd.html).

## Обновление приложения вручную

Для обновления приложения нужно:
1. Пересобрать Docker-образ
2. [Запушить его в registry](/cloud/serverless/registry.html)
3. [Создать новую ревизию контейнера с помощью раздела "Редактор"](/cloud/serverless/container.html)
4. Когда новая ревизия станет со статусом **Active**, можно будет перейти на адрес повторно и посмотреть изменения.