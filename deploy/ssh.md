# Создание SSH ключей

SSH ключи нужны для подключения к серверам без пароля. Доступ проверяется с помощью **приватного ключа**, который лежит у Вас на компьютере, и **публичного ключа**, который кладется на сервер.

::: danger Приватный ключ никому передавать нельзя
С приватным ключом любой человек сможет получить доступ ко всем серверам, к которым Вы подключались.
:::

**Создайте SSH-ключ при помощи команды**
```
ssh-keygen -C "nickname"
```

Во время создания пары ключей, у вас будет запрошен путь и название для сохранения SSH ключей. Оставьте его без изменения (```/Users/user/.ssh/id_rsa```)

Далее вам будет предложено ввести кодовую фразу - оставьте эти поля пустыми
```
ssh-keygen -C "nickname"                     
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/user/.ssh/id_rsa): /Users/user/.ssh/id_rsa
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /Users/user/.ssh/id_rsa
Your public key has been saved in /Users/user/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:3POSy9wAkbwTmDXEVcaZOK9kdS/9ZPxzHUBhkkK2Cjg nickname
The key's randomart image is:
+---[RSA 3072]----+
|       o=+.==*.  |
|    .  =o++oB..  |
|   E .o =..+ ..+ |
|    . ...=o . ..*|
|       .Soo.   +=|
|         o.+   .=|
|          + .   o|
|         o =     |
|          + .    |
+----[SHA256]-----+ 
``` 

Выведите публичный ключ и скопируйте его из консоли:
```cat ~/.ssh/id_rsa.pub```

Теперь вставьте содержимое публичного ключа в интерфейс облачного провайдера (Яндекс.Облако) или в файл `.ssh/authorized_keys`. После этого Вы сможете заходить на сервер без пароля.

## Другие источники
- [Статья в Doka про работу SSH](https://doka.guide/tools/ssh/)
- [Cтатья на Yandex Cloud Docs про создание SSH ключей и подключение](https://cloud.yandex.ru/docs/compute/operations/vm-connect/ssh)
