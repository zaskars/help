# Упаковка проекта в Docker

## Основы
- [Презентация по Docker](https://docs.google.com/presentation/d/1l3wWKsL5A8yJVY2Q8cw8xX99Qg3yX3o3f4zj47ULX0w/edit) + [пример проекта на Django](https://github.com/atnartur/docker-django-example)
- Материалы от Доки
  - [основы Docker](https://doka.guide/tools/docker/)
  - [написание Dockerfile для сборки проекта](https://doka.guide/tools/dockerfile/)
  - [docker compose для запуска группы контейнеров](https://doka.guide/tools/docker-compose/)
  - [управление данными в контейнерах](https://doka.guide/tools/docker-data-management/)

## Примеры проектов

- [Django проект](https://github.com/atnartur/docker-django-example) из [презентации](https://docs.google.com/presentation/d/1l3wWKsL5A8yJVY2Q8cw8xX99Qg3yX3o3f4zj47ULX0w/edit)
- [Django проект c настроенным Gitlab CI](https://gitlab.com/atnartur.uni/django-2k-11-000/)
- [Java проект](https://github.com/rasimusv/examjava2022)
- [.NET core проект](https://github.com/atnartur/docker-dotnet-example) из [презентации](https://docs.google.com/presentation/d/1ROSaOsn-s02x2fTC52lEI625qRUeG4QtxxqSiTzLIq4/edit)

## Больше информации

- [Ссылки на смежные материалы](https://docs.google.com/presentation/d/1nXJBjEBPdKwymeRwk44MFdhUsKNn1-yUy-uPhKwQAoc/edit)
- [Документация по Docker](https://docs.docker.com)
